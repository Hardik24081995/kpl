package com.sprinters.utils.picker

enum class Sources {
    CAMERA, GALLERY, DOCUMENTS, CHOOSER
}